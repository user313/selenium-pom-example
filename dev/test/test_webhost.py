import sys
import time
import pytest
from .test_case_webhost import TestCaseWebHost
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium import webdriver

class TestWebHost(TestCaseWebHost):    
             
    def test_site88(self, example_fixture):
        
        # Print example pytest fixture.
        print(example_fixture + '\n')
        
        # Use POM object to navigate to URL.
        pytest.webhost_page.navigate_to_url()
        
        #close Modal
        pytest.webhost_page.get_textbox().click()

        #Input text iphone into the search box
        elem = pytest.webhost_page.input_text().send_keys('Iphone')
        elem = pytest.webhost_page.input_text().send_keys(Keys.RETURN)

        #page redirects to a second page, adding an explicit wait here
        if pytest.webhost_page.explicit_wait():
            #Click on the compare button, add phones to compare
            pytest.webhost_page.click_compare_button1().click()
            pytest.webhost_page.click_compare_button2().click()
        else:
            pytest.webhost_page.quit()              

        if pytest.webhost_page.explicit_wait1():
            print("Clicking on the COmpat Button#################################")
            #Click on the compate link button
            pytest.webhost_page.click_compare_route().click()
        else:
            pytest.webhost_page.quit()

        #click compare differences only button
        if pytest.webhost_page.differences_select_button():
            print("Element Found")
            pytest.webhost_page.differences_select_button_click().click()


        if pytest.webhost_page.get_compare_details():
            print("Capturing compare details of the phone")

        # Wait before closing browser.
        TestCaseWebHost.wait_before_closing_browser(self)