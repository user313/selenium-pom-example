from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium import webdriver

SITE_URL = 'https://www.flipkart.com/'

class Site88Page:
            
    def __init__(self, driver):
        self.driver = driver
        
    #Locators    
    _compare_1_ = "//*[@id='container']/div/div[1]/div[2]/div/div[1]/div[2]/div[2]/div/div/div/a/div[2]/div[2]/div"
    _compare_2_ = "//*[@id='container']/div/div[1]/div[2]/div/div[1]/div[2]/div[3]/div/div/div/a/div[2]/div[2]/div"
    _compare_but = "//*[@id='container']/div/div[1]/div[2]/div/div[3]/div/a[contains(.,compare)]"
    _compare_details = "//*[@id='fk-compare-page']/div/div/div/div[2]/div[1]"
    _difference_button = "//*[@id='fk-compare-page']/div/div/div/div[1]/div[2]/div/div[1]/div[3]/div[1]/div/span[1]/div/label/div"

    def navigate_to_url(self):
        self.driver.get(SITE_URL)
        
    def get_textbox(self):
        return self.driver.find_element_by_xpath("/html/body/div[2]/div/div/button")

    def input_text(self):
        return self.driver.find_element_by_xpath("//*[@id='container']/div/header/div[1]/div/div[2]/form/div/div[1]/div/input")

    def click_compare_button1(self):
        return self.driver.find_element_by_xpath(self._compare_1_)
    
    def click_compare_button2(self):
        return self.driver.find_element_by_xpath(self._compare_2_)   

    def click_compare_route(self):
        return self.driver.find_element_by_xpath(self._compare_but)

    #function can be upgraded to a wrapper when we can accept a wait, hardcoding for now.
    def explicit_wait(self):
        element = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.XPATH, self._compare_1_)))
        if element:
            return True
        else:
            return False

    def explicit_wait1(self):
        element = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.XPATH, self._compare_but)))
        if element:
            return True
        else:
            return False

    def differences_select_button(self):
        element = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.XPATH, self._difference_button)))
        if element:
            return True
        else:
            return False

    def differences_select_button_click(self):
        return self.driver.find_element_by_xpath(self._difference_button)

    def get_compare_details(self):
        element = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.XPATH,self._compare_details)))
        if element:
            return True
        else:
            return False